# Fix Report of Group 24

## Phase 2 Deadline: 08Nov2020-23h59m)

Group Image available [here](http://ea40bfc5f8cb1209bc8e82a9fef82e77123a0845ee6a784ba7e386411b01.project.ssof.rnl.tecnico.ulisboa.pt/)

- Vulnerabilities in `/`:
    1. Loading feed after another user performs an update allows you to edit their posts
        - Root cause: `current_user` global variable was used in certain pages, sharing state between sessions
        - Changes: Removed the `current_user` global and changed the app to match it.

- Vulnerabilities in `/login`:
    1. SQL injection in parameter `username` allows reading posts and profile of other users
        - Root cause: format strings were used rather than prepared statements
        - Changes: Fixed function `login_user` by using prepared statements

    2. SQL injection in parameter `password` allows full access to other users' accounts
        - Root cause: format strings were used rather than prepared statements
        - Changes: Fixed function `login_user` by using prepared statements

- Vulnerabilities in `/register`:
    1. HTML injection in parameter `username` allows for defacing
        - Root cause: unsanitized input
        - Changes: Escaped `username` field in all views

    2. SQL injection in parameter `username` allows for execution of any query on the database
        - Root cause: format strings were used rather than prepared statements
        - Changes: Fixed function `register_user` by using prepared statements

    3. SQL injection in parameter `password` allows for execution of any query on the database
        - Root cause: format strings were used rather than prepared statements
        - Changes: Fixed function `register_user` by using prepared statements

- Vulnerabilities in `/profile`:
    1. SQL injection in parameters `about`, `name`, `newpassword` and `photo` allow for changing other user’s info
        - Root cause: format strings were used rather than prepared statements
        - Changes: Fixed function `update_user` by using prepared statements

    2. Stored XSS in parameters `name` and `about` allows for defacing
        - Root cause: user inserted input was not escaped before posting to the output
        - Changes:
            1. Fixed template `base.html` by escaping `name` parameter in the template
            2. Fixed template `friends.html` by escaping `name` and `about` parameters in the template
            3. Fixed template `home.html` by escaping `author` parameter in the template
            4. Fixed template `pending_requests.html` by escaping `name` and `about` parameters in the template
            5. Fixed template `profile.html` by escaping `name` and `about` parameters in the template

    3. Stored XSS + CSRF in parameters `name` and `about` allows for making requests as another user
        - Root cause: user inserted input was not escaped before posting to the output
        - Changes:
            1. Fixed template `base.html` by escaping `name` parameter in the template
            2. Fixed template `friends.html` by escaping `name` and `about` parameters in the template
            3. Fixed template `home.html` by escaping `author` parameter in the template
            4. Fixed template `pending_requests.html` by escaping `name` and `about` parameters in the template
            5. Fixed template `profile.html` by escaping `name` and `about` parameters in the template

- Vulnerabilities in `/create_post`:
    1. SQL injection in `content` field allows for execution of any query on the database
        - Root cause: format strings were used rather than prepared statements
        - Changes:
            1. Fixed function `new_post` by using prepared statements
    2. Stored XSS in parameter `content` allows for home page defacing
        - Root cause: user inserted input was not escaped before posting to the output
        - Changes:
            1. Fixed template `home.html` by escaping the parameters of the template
    3. Stored XSS + CSRF in the content of the posts
        - Root cause: user inserted input was not escaped before posting to the output
        - Changes:
            1. Fixed template `home.html` by escaping the parameters of the template

- Vulnerablities in `/edit_post`:
    1. Broken Access Control allows reading of any post
        - Root cause: lack of access control when getting the post to edit
        - Changes:
            1. Fixed function `edit_post`(on views.py) by doing propper access control(checking if user created the post) on `GET`

    2. Broken Access Control allows editing of any post
        - Root cause: lack of access control when editting the post
        - Changes:
            1. Fixed function `edit_post`(on views.py) by doing propper access control(checking if user created the post) on `POST`

    3. SQLi allows editing of all posts
        - Root cause: format strings were used rather than prepared statements
        - Changes:
            1. Fixed function `get_post` by using prepared statements
            2. Fixed function `edit_post` by using prepared statements

    4. Stored XSS in the content of the posts
        - Root cause: user inserted input was not escaped before posting to the output
        - Changes:
            1. Fixed template `home.html` by escaping the parameters of the template

    5. Stored XSS + CSRF in the content of the posts
        - Root cause: user inserted input was not escaped before posting to the output
        - Changes:
            1. Fixed template `home.html` by escaping the parameters of the template

- Vulnerabilities in `/friends`:
    1. SQL injection in field `search` allows gathering of any information  (that the db user has access to) from the database
        - Root cause: format strings were used rather than prepared statements
        - Changes: Fixed function `get_friends` by using prepared statements

- Vulnerabilities in `/request_friends`:
    1. SQL injection in parameter `username` allows for execution of any query on the database
        - Root cause: format strings were used rather than prepared statements
        - Changes: Fixed function `new_friend_request` by using prepared statements
        - Changes: Fixed function `get_user` by using prepared statements

- Vulnerabilities in `/pending_requests`:
    1. SQL injection in hidden parameter `username` allows for execution of any query on the database
        - Root cause: format strings were used rather than prepared statements
        - Changes:
            1. Fixed function `is_request_pending` by using prepared statements
            2. Fixed function `get_pending_requests` by using prepared statements
            3. Fixed function `accept_friend_request` by using prepared statements

- Extra:
    1. Insecure password storage
        - Root cause: passwords were stored in plain text and compared without case sensetivity
        - Changes: use `bcrypt` library to hash the passwords and test them against the saved hash
        - Note: username matching was changed to be case sensitive too

    2. CSRF
        - Root cause: No protections against CSRF were used
        - Changes:
            1. Added CSRF tokens for `POST` requests when:
                - Creating a post
                - Editing a post
                - Making a friend request
                - Accepting a friend request
            2. Usage of the `SameSite` flag in cookies, with value `Lax`

    3. Database errors
        - Root cause: Database errors were sent to the output
        - Changes:
            1. Changed error messages in the following functions of `views.py`:
                - `home`
                - `login`
                - `register`
                - `update_profile`
                - `create_post`
                - `edit_post`
                - `request_friend`
                - `pending_requests`
                - `friends`

    4. Upload photo HTML execution
        - Root cause: if user sent html file, could run JS by accessing image URL
        - Changes: only allow image extensions (will not execute the content)

    5. Max file size
        - Root cause: unlimited image size upload could lead to Denial of Service
        - Changes: set maximum image size upload to 100KiB

     6. Possible path traversal
         - Root cause: unproperly sanitized uploaded photos filename
         - Changes: use `secure_filename` before saving image

    7. Denial of Service in DB
        - Root cause: unlimited storage for fields `name` and `about` in table `Users` and for field `content` in table `Posts`
        - Changes: use `VARCHAR(value)` instead of `TEXT`

## Notes

- __Use the same numbering that you used for Phase 1.__
- Edit the files directly so that in the final commit there are no vulnerabilities in your application.
- Refer in each commit which vulnerability is fixed.
- Test your own PoCs against the fixed application in order to verify that the vulnerabilities are no longer present (the `assert`s in the end should now fail).
- If you find vulnerabilities that you did not exploit, you may also fix those.
